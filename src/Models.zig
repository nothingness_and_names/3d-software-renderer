const std = @import("std");
const vector = @import("vector_math.zig");
const colors = @import("colors.zig");
const Textures = @import("Textures.zig");
const ObjParser = @import("ObjParser.zig");

const ArrayList = std.ArrayList;
const AutoHashMap = std.AutoHashMap;
const Color = colors.Color;
const Vec3 = vector.Vec3;

const sqrt = std.math.sqrt;

const pi: f32 = 3.141592653589;

var buffer: [5000 * 1024]u8 = undefined;
var fba = std.heap.FixedBufferAllocator.init(&buffer);
const allocator = fba.allocator();

var scratch_buffer: [1000 * 1024]u8 = undefined;
var scratch_fba = std.heap.FixedBufferAllocator.init(&scratch_buffer);
const scratch_allocator = scratch_fba.allocator();

pub const Triangle = struct {
    indices: [3]u16,
    normals: ?u16,
    uv_data: ?u16,
};

pub const ModelTag = enum {
    box,
    sphere,
    capsule,
};

pub const Model = struct {
    vertices: ArrayList(Vec3),
    triangles: ArrayList(Triangle),
    normals: ArrayList([3]Vec3),
    uvs: ArrayList([3][2]f32),
    center: Vec3,
    bounding_radius_squared: f32,
    texture: Textures.TextureTag,
};

pub const ExtraData = union {
    normals: [3]Vec3,
    uv_data: [3][2]f32,
};

pub var models = AutoHashMap(ModelTag, Model).init(allocator);

pub fn loadBox() !void {
    const vertices = [8]Vec3{
        .{ .x = 1, .y = 1, .z = 1 },
        .{ .x = -1, .y = 1, .z = 1 },
        .{ .x = -1, .y = -1, .z = 1 },
        .{ .x = 1, .y = -1, .z = 1 },
        .{ .x = 1, .y = 1, .z = -1 },
        .{ .x = -1, .y = 1, .z = -1 },
        .{ .x = -1, .y = -1, .z = -1 },
        .{ .x = 1, .y = -1, .z = -1 },
    };

    var triangles = [12]Triangle{
        .{ .indices = [3]u16{ 0, 1, 2 }, .normals = null, .uv_data = null },
        .{ .indices = [3]u16{ 0, 2, 3 }, .normals = null, .uv_data = null },
        .{ .indices = [3]u16{ 4, 0, 3 }, .normals = null, .uv_data = null },
        .{ .indices = [3]u16{ 4, 3, 7 }, .normals = null, .uv_data = null },
        .{ .indices = [3]u16{ 5, 4, 7 }, .normals = null, .uv_data = null },
        .{ .indices = [3]u16{ 5, 7, 6 }, .normals = null, .uv_data = null },
        .{ .indices = [3]u16{ 1, 5, 6 }, .normals = null, .uv_data = null },
        .{ .indices = [3]u16{ 1, 6, 2 }, .normals = null, .uv_data = null },
        .{ .indices = [3]u16{ 1, 0, 5 }, .normals = null, .uv_data = null },
        .{ .indices = [3]u16{ 5, 0, 4 }, .normals = null, .uv_data = null },
        .{ .indices = [3]u16{ 2, 6, 7 }, .normals = null, .uv_data = null },
        .{ .indices = [3]u16{ 2, 7, 3 }, .normals = null, .uv_data = null },
    };

    const uv_coords = [12][3][2]f32{
        [3][2]f32{ [2]f32{ 0, 0 }, [2]f32{ 1, 0 }, [2]f32{ 1, 1 } },
        [3][2]f32{ [2]f32{ 0, 0 }, [2]f32{ 1, 1 }, [2]f32{ 0, 1 } },
        [3][2]f32{ [2]f32{ 0, 0 }, [2]f32{ 1, 0 }, [2]f32{ 1, 1 } },
        [3][2]f32{ [2]f32{ 0, 0 }, [2]f32{ 1, 1 }, [2]f32{ 0, 1 } },
        [3][2]f32{ [2]f32{ 0, 0 }, [2]f32{ 1, 0 }, [2]f32{ 1, 1 } },
        [3][2]f32{ [2]f32{ 0, 0 }, [2]f32{ 1, 1 }, [2]f32{ 0, 1 } },
        [3][2]f32{ [2]f32{ 0, 0 }, [2]f32{ 1, 0 }, [2]f32{ 1, 1 } },
        [3][2]f32{ [2]f32{ 0, 0 }, [2]f32{ 1, 1 }, [2]f32{ 0, 1 } },
        [3][2]f32{ [2]f32{ 1, 0 }, [2]f32{ 1, 1 }, [2]f32{ 0, 0 } },
        [3][2]f32{ [2]f32{ 0, 0 }, [2]f32{ 1, 1 }, [2]f32{ 0, 1 } },
        [3][2]f32{ [2]f32{ 0, 0 }, [2]f32{ 1, 0 }, [2]f32{ 1, 1 } },
        [3][2]f32{ [2]f32{ 0, 0 }, [2]f32{ 1, 1 }, [2]f32{ 0, 1 } },
    };

    var model = Model{
        .vertices = ArrayList(Vec3).init(allocator),
        .triangles = ArrayList(Triangle).init(allocator),
        .normals = ArrayList([3]Vec3).init(allocator),
        .uvs = ArrayList([3][2]f32).init(allocator),
        .center = .{ .x = 0, .y = 0, .z = 0 },
        .bounding_radius_squared = 3.0,
        .texture = .crate,
    };

    loadModelTexture(model.texture) catch |err| {
        std.debug.print("Texture load error: {}\n", .{err});
        model.texture = .none;
    };

    for (vertices) |v| {
        try model.vertices.append(v);
    }

    for (triangles) |*t, i| {
        const coords = uv_coords[i];

        model.uvs.append(coords) catch |err| {
            std.debug.print("Model load error: {}\n", .{err});
            continue;
        };

        const last_idx = model.uvs.items.len - 1;
        t.*.uv_data = @intCast(u16, last_idx);

        try model.triangles.append(t.*);
    }

    try models.put(.box, model);
}

pub fn loadSphere() !void {
    var model = Model{
        .vertices = try ArrayList(Vec3).initCapacity(allocator, 2400),
        .triangles = try ArrayList(Triangle).initCapacity(allocator, 2400),
        .normals = ArrayList([3]Vec3).init(allocator),
        .uvs = ArrayList([3][2]f32).init(allocator),
        .center = .{ .x = 0, .y = 0, .z = 0 },
        .bounding_radius_squared = 3.0,
        .texture = .none,
    };

    const divs: u16 = 48;
    const divs_f = @intToFloat(f32, divs);

    const delta_angle = 2 * pi / divs_f;

    var i: u16 = 0;
    while (i < divs + 1) : (i += 1) {
        const i_f = @intToFloat(f32, i);

        const y = (2 / divs_f) * (i_f - (divs_f / 2));
        const radius = sqrt(1 - y * y);

        var j: u16 = 0;
        while (j < divs) : (j += 1) {
            const j_f = @intToFloat(f32, j);

            const vert = Vec3{
                .x = radius * @cos(j_f * delta_angle),
                .y = y,
                .z = radius * @sin(j_f * delta_angle),
            };

            try model.vertices.append(vert);
        }
    }

    i = 0;
    while (i < divs) : (i += 1) {
        var j: u8 = 0;
        while (j < divs) : (j += 1) {
            const a = i * divs + j;
            const b = (i + 1) * divs + (j + 1) % divs;
            const c = divs * i + (j + 1) % divs;

            const tri0 = [3]u16{ @as(u16, a), @as(u16, b), @as(u16, c) };
            const tri1 = [3]u16{ @as(u16, a), @as(u16, a + divs), @as(u16, b) };

            const n00 = model.vertices.items[tri0[0]];
            const n01 = model.vertices.items[tri0[1]];
            const n02 = model.vertices.items[tri0[2]];

            const normals0 = [3]Vec3{ n00, n01, n02 };

            try model.normals.append(normals0);

            try model.triangles.append(.{
                .indices = tri0,
                .normals = @intCast(u16, model.normals.items.len - 1),
                .uv_data = null,
            });

            const n10 = model.vertices.items[tri1[0]];
            const n11 = model.vertices.items[tri1[1]];
            const n12 = model.vertices.items[tri1[2]];

            const normals1 = [3]Vec3{ n10, n11, n12 };
            try model.normals.append(normals1);

            try model.triangles.append(.{
                .indices = tri1,
                .normals = @intCast(u16, model.normals.items.len - 1),
                .uv_data = null,
            });
        }
    }

    try models.put(.sphere, model);
}

pub fn loadModelFromObj(tag: ModelTag) !void {
    const path = getModelPath(tag);

    if (path == null) {
        std.debug.print("No file exists for model\n", .{});
        return;
    }

    var model = Model{
        .vertices = ArrayList(Vec3).init(allocator),
        .triangles = ArrayList(Triangle).init(allocator),
        .normals = ArrayList([3]Vec3).init(allocator),
        .uvs = ArrayList([3][2]f32).init(allocator),
        .center = .{ .x = 0, .y = 0, .z = 0 },
        .bounding_radius_squared = 3,
        .texture = .none,
    };

    var temp_normals = ArrayList(Vec3).init(scratch_allocator);
    var temp_uvs = ArrayList([2]f32).init(scratch_allocator);
    defer scratch_fba.reset();

    const file = try std.fs.cwd().openFile(path.?, .{});
    defer file.close();

    var reader = std.io.bufferedReader(file.reader()).reader();

    var buf: [1000]u8 = undefined;
    var line = try reader.readUntilDelimiterOrEof(&buf, '\n');

    var count: usize = 0;
    while (line != null) {
        try ObjParser.parseLine(line.?, &model, &temp_normals, &temp_uvs);
        count += 1;

        line = try reader.readUntilDelimiterOrEof(&buf, '\n');
    }

    try models.put(tag, model);
}

fn loadModelTexture(tag: Textures.TextureTag) !void {
    const existing = Textures.textures.get(tag);

    if (existing != null) return;

    try Textures.loadTexture(tag);
}

fn getModelPath(tag: ModelTag) ?[]const u8 {
    switch (tag) {
        .capsule => {
            return "res/models/capsule.obj";
        },
        .box, .sphere => {
            return null;
        },
    }
}
