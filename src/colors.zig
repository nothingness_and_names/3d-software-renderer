const std = @import("std");

const sqrt = std.math.sqrt;

pub const Color = struct {
    r: u8,
    g: u8,
    b: u8,

    pub fn pack(self: Color, format: PixelFormat) u32 {
        switch (format) {
            .rgb888 => {
                const red = @as(u32, self.r);
                const green = @as(u32, self.g);
                const blue = @as(u32, self.b);

                return (red << 16) | (green << 8) | blue;
            },
        }
    }

    pub fn convertToLinear(self: Color) Color {
        const f_red = @intToFloat(f32, self.r);
        const f_green = @intToFloat(f32, self.g);
        const f_blue = @intToFloat(f32, self.b);

        const inv255 = 1.0 / 255.0;

        const max_1_red = f_red * inv255;
        const max_1_green = f_green * inv255;
        const max_1_blue = f_blue * inv255;

        const result_red = max_1_red * max_1_red;
        const result_green = max_1_green * max_1_green;
        const result_blue = max_1_blue * max_1_blue;

        const clamped_red = clampFloat(result_red * 255, 0, 255);
        const clamped_green = clampFloat(result_green * 255, 0, 255);
        const clamped_blue = clampFloat(result_blue * 255, 0, 255);

        return Color{
            .r = @floatToInt(u8, clamped_red),
            .g = @floatToInt(u8, clamped_green),
            .b = @floatToInt(u8, clamped_blue),
        };
    }

    pub fn convertToSRGB(self: Color) Color {
        const f_red = @intToFloat(f32, self.r);
        const f_green = @intToFloat(f32, self.g);
        const f_blue = @intToFloat(f32, self.b);

        const inv255 = 1.0 / 255.0;

        const max_1_red = f_red * inv255;
        const max_1_green = f_green * inv255;
        const max_1_blue = f_blue * inv255;

        const result_red = sqrt(max_1_red);
        const result_green = sqrt(max_1_green);
        const result_blue = sqrt(max_1_blue);

        const clamped_red = clampFloat(result_red * 255, 0, 255);
        const clamped_green = clampFloat(result_green * 255, 0, 255);
        const clamped_blue = clampFloat(result_blue * 255, 0, 255);

        return Color{
            .r = @floatToInt(u8, clamped_red),
            .g = @floatToInt(u8, clamped_green),
            .b = @floatToInt(u8, clamped_blue),
        };
    }

    pub fn multiplyFloat(self: Color, coeff: f32) Color {
        const f_red = @intToFloat(f32, self.r);
        const f_green = @intToFloat(f32, self.g);
        const f_blue = @intToFloat(f32, self.b);

        const result_red = clampFloat(f_red * coeff, 0, 255);
        const result_green = clampFloat(f_green * coeff, 0, 255);
        const result_blue = clampFloat(f_blue * coeff, 0, 255);

        return .{
            .r = @floatToInt(u8, result_red),
            .g = @floatToInt(u8, result_green),
            .b = @floatToInt(u8, result_blue),
        };
    }

    pub fn add(self: Color, color: Color) Color {
        const red = clampInt(@as(u32, self.r) + @as(u32, color.r), 0, 255);
        const green = clampInt(@as(u32, self.g) + @as(u32, color.g), 0, 255);
        const blue = clampInt(@as(u32, self.b) + @as(u32, color.b), 0, 255);

        return .{
            .r = @intCast(u8, red),
            .g = @intCast(u8, green),
            .b = @intCast(u8, blue),
        };
    }
};

pub const PixelFormat = enum { rgb888 };

pub fn fromScreenValue(pixel: u32, fmt: PixelFormat) Color {
    switch (fmt) {
        .rgb888 => {
            const red = (pixel & 0xFF0000) >> 16;
            const green = (pixel & 0xFF00) >> 8;
            const blue = (pixel & 0xFF);

            return Color{
                .r = @intCast(u8, red),
                .g = @intCast(u8, green),
                .b = @intCast(u8, blue),
            };
        },
    }
}

fn clampInt(value: u32, min: u32, max: u32) u32 {
    var result = value;
    if (value > max) result = max;
    if (value < min) result = min;

    return result;
}

fn clampFloat(value: f32, min: f32, max: f32) f32 {
    var result = value;
    if (value > max) result = max;
    if (value < min) result = min;

    return result;
}
