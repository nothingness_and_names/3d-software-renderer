const std = @import("std");
const qoi = @import("zig-qoi");
const colors = @import("colors.zig");

const AutoHashMap = std.AutoHashMap;
const ArrayList = std.ArrayList;

var buffer: [5000 * 1024]u8 = undefined;
var fba = std.heap.FixedBufferAllocator.init(&buffer);
const allocator = fba.allocator();

var scratch_buffer: [2000 * 1024]u8 = undefined;
var scratch_fba = std.heap.FixedBufferAllocator.init(&scratch_buffer);
const scratch_allocator = scratch_fba.allocator();

pub const TextureTag = enum {
    test_texture,
    crate,
    numbers,
    none,
};

pub const Texel = struct {
    color: colors.Color,
    alpha: u8,
};

pub const Texture = struct {
    texels: ArrayList(Texel),
    width: u32,
    height: u32,
};

pub var textures = AutoHashMap(TextureTag, Texture).init(allocator);

pub fn loadTexture(tag: TextureTag) !void {
    const file = try std.fs.cwd().openFile(getTexturePath(tag), .{});
    defer file.close();

    var stream = std.io.bufferedReader(file.reader());

    const image = try qoi.decodeStream(scratch_allocator, stream.reader());
    defer scratch_fba.reset();

    var texture = Texture{
        .texels = ArrayList(Texel).init(allocator),
        .width = image.width,
        .height = image.height,
    };

    for (image.pixels) |color| {
        const tex_color = colors.Color{
            .r = color.r,
            .g = color.g,
            .b = color.b,
        };
        const alpha = color.a;

        const texel = Texel{
            .color = tex_color.convertToLinear().multiplyFloat(@intToFloat(f32, alpha) / 255.0),
            .alpha = alpha,
        };

        try texture.texels.append(texel);
    }

    try textures.put(tag, texture);
}

fn getTexturePath(tag: TextureTag) []const u8 {
    switch (tag) {
        .test_texture => {
            return "res/textures/test.qoi";
        },
        .crate => {
            return "res/textures/crate.qoi";
        },
        .numbers => {
            return "res/textures/numbers.qoi";
        },
        .none => {
            return "res/textures/missing.qoi";
        },
    }
}
