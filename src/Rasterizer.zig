const std = @import("std");
const vector = @import("vector_math.zig");
const Input = @import("Input.zig");
const colors = @import("colors.zig");
const Models = @import("Models.zig");
const Textures = @import("Textures.zig");
const lerp = @import("lerp.zig");

const ArrayList = std.ArrayList;
const Vec2 = vector.Vec2;
const Vec3 = vector.Vec3;
const Plane = vector.Plane;
const Quaternion = vector.Quaternion;
const Color = colors.Color;
const PixelFormat = colors.PixelFormat;
const Allocator = std.mem.Allocator;
const Fba = std.heap.FixedBufferAllocator;

const sqrt2: f32 = 1.41421356;

var thread1_buffer: [300 * 1024]u8 = undefined;
var thread1_fba = std.heap.FixedBufferAllocator.init(&thread1_buffer);
const thread1_allocator = thread1_fba.allocator();

var thread2_buffer: [300 * 1024]u8 = undefined;
var thread2_fba = std.heap.FixedBufferAllocator.init(&thread2_buffer);
const thread2_allocator = thread2_fba.allocator();

var thread3_buffer: [300 * 1024]u8 = undefined;
var thread3_fba = std.heap.FixedBufferAllocator.init(&thread3_buffer);
const thread3_allocator = thread3_fba.allocator();

var thread4_buffer: [300 * 1024]u8 = undefined;
var thread4_fba = std.heap.FixedBufferAllocator.init(&thread4_buffer);
const thread4_allocator = thread4_fba.allocator();

const frame_allocator = std.heap.c_allocator;

// width = 2 * tan(fov / 2) * distance
const viewport_width: f32 = 1.333333;
const viewport_height: f32 = 1.0;
const projection_plane_d: f32 = 0.87;

pub const Screen = struct {
    screen_buffer: []u32,
    depth_buffer: []f32,
    pixel_format: PixelFormat,
};

const ScreenSegment = struct {
    min_x: i16,
    max_x: i16,
    min_y: i16,
    max_y: i16,
};

const Triangle = struct {
    vertices: [3]Vec3,
    normals: ?u16,
    uv_data: ?u16,
};

const ClippingResult = struct {
    positive_count: u32,
    negative_count: u32,
    positive_index: usize,
    negative_index: usize,
};

const Object = struct {
    model: Models.ModelTag,
    position: Vec3,
    rotation: Vec3,
    scale: f32,

    fn makeRotationMatrix(self: Object) [4][4]f32 {
        const rotation = self.rotation;
        const quat_x = vector.createQuaternion(.{ .x = 1, .y = 0, .z = 0 }, rotation.x);
        const quat_y = vector.createQuaternion(.{ .x = 0, .y = 1, .z = 0 }, rotation.y);
        const quat_z = vector.createQuaternion(.{ .x = 0, .y = 0, .z = 1 }, rotation.z);

        const x_z_rotation = quat_x.hamiltonProduct(quat_z);

        const total_rotation = quat_y.hamiltonProduct(x_z_rotation);

        return total_rotation.toRotationMatrix();
    }

    fn makeTransformMatrix(self: Object) [4][4]f32 {
        const scale = self.scale;
        const scale_matrix = [4][4]f32{
            [4]f32{ scale, 0, 0, 0 },
            [4]f32{ 0, scale, 0, 0 },
            [4]f32{ 0, 0, scale, 0 },
            [4]f32{ 0, 0, 0, 1 },
        };

        const rotation_matrix = self.makeRotationMatrix();
        const rotation_and_scale = vector.multiplyMM4(rotation_matrix, scale_matrix);

        const translation_matrix = self.position.toTranslationMatrix();

        return vector.multiplyMM4(translation_matrix, rotation_and_scale);
    }
};

const LightType = enum {
    ambient,
    point,
    directional,
};

const Light = struct {
    tag: LightType,
    intensity: f32,
    vector: Vec3,
};

var screen_width: u16 = 0;
var screen_height: u16 = 0;
var max_screen_x: i16 = 0;
var max_screen_y: i16 = 0;
var camera_position: Vec3 = .{ .x = 0, .y = 0, .z = 0 };
var camera_y_angle: f32 = 0.0;
var camera_x_angle: f32 = 0.0;

var transposed_camera_rotation: [4][4]f32 = undefined;
var camera_matrix: [4][4]f32 = undefined;

var objects: [3]Object = undefined;
var lights: [2]Light = undefined;
const clipping_planes = [5]Plane{
    .{ .normal = .{ .x = 0, .y = 0, .z = 1 }, .distance = -1 },
    .{ .normal = .{ .x = sqrt2, .y = 0, .z = sqrt2 }, .distance = 0 },
    .{ .normal = .{ .x = -sqrt2, .y = 0, .z = sqrt2 }, .distance = 0 },
    .{ .normal = .{ .x = 0, .y = -sqrt2, .z = sqrt2 }, .distance = 0 },
    .{ .normal = .{ .x = 0, .y = sqrt2, .z = sqrt2 }, .distance = 0 },
};

pub fn initialize(width: u16, height: u16) !void {
    screen_width = width;
    screen_height = height;
    max_screen_x = @intCast(i16, width / 2);
    max_screen_y = @intCast(i16, height / 2);

    try Models.loadBox();
    Models.loadModelFromObj(.capsule) catch |err| {
        std.debug.print("Failed to load model from file: {}\n", .{err});
    };

    Textures.loadTexture(.numbers) catch |err| {
        std.debug.print("Failed to load frame timer textures: {}\n", .{err});
    };

    const obj1 = Object{
        .model = .box,
        .position = .{ .x = -1.5, .y = 1.5, .z = 7 },
        .rotation = .{ .x = 0, .y = 0, .z = 0 },
        .scale = 0.75,
    };
    const obj2 = Object{
        .model = .box,
        .position = .{ .x = 1.25, .y = 2, .z = 7.5 },
        .rotation = .{ .x = 0.0, .y = 3.4, .z = 0 },
        .scale = 1,
    };

    const obj3 = Object{
        .model = .capsule,
        .position = .{ .x = 1.25, .y = -1, .z = 7.5 },
        .rotation = .{ .x = 0.0, .y = 0, .z = 0 },
        .scale = 1,
    };

    objects[0] = obj1;
    objects[1] = obj2;
    objects[2] = obj3;

    const light1 = Light{
        .tag = .ambient,
        .intensity = 0.1,
        .vector = .{ .x = 0, .y = 0, .z = 0 },
    };
    const light2 = Light{
        .tag = .point,
        .intensity = 0.5,
        .vector = .{ .x = -1.5, .y = 1.5, .z = 5 },
    };

    lights[0] = light1;
    lights[1] = light2;
}

pub fn renderScene(screen: *Screen, dt: f32) !void {
    clearScreen(screen);
    processInput();

    makeCameraMatrix();

    try renderObjects(screen);

    drawFrameTimer(screen, dt);
}

fn makeCameraMatrix() void {
    const camera_rotation_y = vector.createQuaternion(.{ .x = 0, .y = 1, .z = 0 }, camera_y_angle);
    const camera_rotation_x = vector.createQuaternion(.{ .x = 1, .y = 0, .z = 0 }, camera_x_angle);
    const camera_rotation = camera_rotation_y.hamiltonProduct(camera_rotation_x)
        .toRotationMatrix();
    transposed_camera_rotation = vector.transposeMatrix44(camera_rotation);

    const camera_translation = camera_position.multiply(-1).toTranslationMatrix();

    camera_matrix = vector.multiplyMM4(transposed_camera_rotation, camera_translation);
}

fn renderObjects(screen: *Screen) !void {
    const seg1 = .{ .min_x = -max_screen_x, .min_y = -max_screen_y + 1, .max_x = 0, .max_y = 0 };
    const seg2 = .{ .min_x = 0, .min_y = -max_screen_y + 1, .max_x = max_screen_x, .max_y = 0 };
    const seg3 = .{ .min_x = -max_screen_x, .min_y = 0, .max_x = 0, .max_y = max_screen_y };
    const seg4 = .{ .min_x = 0, .min_y = 0, .max_x = max_screen_x, .max_y = max_screen_y };

    var frame1 = try frame_allocator.create(@Frame(renderObjectsWorker));
    var frame2 = try frame_allocator.create(@Frame(renderObjectsWorker));
    var frame3 = try frame_allocator.create(@Frame(renderObjectsWorker));
    var frame4 = try frame_allocator.create(@Frame(renderObjectsWorker));
    defer frame_allocator.destroy(frame1);
    defer frame_allocator.destroy(frame2);
    defer frame_allocator.destroy(frame3);
    defer frame_allocator.destroy(frame4);

    frame1.* = async renderObjectsWorker(screen, seg1, thread1_allocator, &thread1_fba);
    frame2.* = async renderObjectsWorker(screen, seg2, thread2_allocator, &thread2_fba);
    frame3.* = async renderObjectsWorker(screen, seg3, thread3_allocator, &thread3_fba);
    frame4.* = async renderObjectsWorker(screen, seg4, thread4_allocator, &thread4_fba);

    _ = await frame1;
    _ = await frame2;
    _ = await frame3;
    _ = await frame4;
}

fn renderObjectsWorker(screen: *Screen, segment: ScreenSegment, allocator: Allocator, fba: *Fba) !void {
    std.event.Loop.startCpuBoundOperation();

    for (objects) |obj| {
        const transform = vector.multiplyMM4(camera_matrix, obj.makeTransformMatrix());
        try renderObject(screen, obj, transform, segment, allocator, fba);
    }
}

// zig fmt: off
fn renderObject(screen: *Screen, obj: Object, transform: [4][4]f32, 
                segment: ScreenSegment, scratch_allocator: Allocator, scratch_fba: *Fba) !void {
    // zig fmt: on
    const model = Models.models.get(obj.model) orelse return;
    const texture = Textures.textures.get(model.texture);

    const model_center = vector.multiplyMV4(transform, model.center.toHomogeneousVertex());
    const transformed_center = vector.homogeneousToVec(model_center);

    // If bounding sphere is entirely outside of any clipping planes, don't render
    for (clipping_planes) |plane| {
        const distance_squared = plane.normal.dot(transformed_center) + plane.distance;

        if (distance_squared < -model.bounding_radius_squared) {
            return;
        }
    }

    var transformed = ArrayList(Vec3).init(scratch_allocator);
    var visible_triangles = ArrayList(Models.Triangle).init(scratch_allocator);
    var clipped_triangles = ArrayList(Triangle).init(scratch_allocator);
    var normals = ArrayList([3]Vec3).init(scratch_allocator);
    var uv_data = ArrayList([3][2]f32).init(scratch_allocator);
    var should_remove_triangle = ArrayList(bool).init(scratch_allocator);
    defer scratch_fba.reset();

    for (model.vertices.items) |v| {
        const transformed_v = vector.multiplyMV4(transform, v.toHomogeneousVertex());
        const new_v = vector.homogeneousToVec(transformed_v);

        try transformed.append(new_v);
    }

    for (model.triangles.items) |tri| {
        const plane = clipping_planes[0];

        const v = [3]Vec3{
            transformed.items[tri.indices[0]],
            transformed.items[tri.indices[1]],
            transformed.items[tri.indices[2]],
        };

        // Back face fulling
        const normal = computeTriangleNormal(v[0], v[1], v[2]);
        const center = v[0].add(v[1].add(v[2])).multiply(-1.0 / 3.0);

        if (center.dot(normal) <= 0) continue;

        const result = analyzeTriangleClipping(plane, v);

        if (result.positive_count == 3) {
            try visible_triangles.append(tri);
        } else if (result.positive_count == 1) {
            // zig fmt: off
            const start_idx = result.positive_index;
            const clipped = try createClippedTriangle(model, v, start_idx, plane, tri.normals, 
                                                      tri.uv_data, &normals, &uv_data, false);
            try clipped_triangles.append(clipped);
        } else if (result.negative_count == 1) {
            const start_idx = result.negative_index;
            const two_clipped = try createTwoClippedTriangles(model, v, start_idx, plane, tri.normals, 
                                                              tri.uv_data, &normals, &uv_data, false);

            // zig_fmt: on
            for (two_clipped) |clipped| {
                try clipped_triangles.append(clipped);
            }
        }
    }

    const remaining_planes = clipping_planes[1..clipping_planes.len];
    for (remaining_planes) |plane| {
        // Find which previously clipped triangles need to be removed / reclipped
        var addtl_clipped_count: u32 = 0;
        for (clipped_triangles.items) |cl| {
            const result = analyzeTriangleClipping(plane, cl.vertices);

            if (result.positive_count != 3) {
                try should_remove_triangle.append(true);
            } else {
                try should_remove_triangle.append(false);
            }

            if (result.positive_count == 1) {
                // zig fmt: off
                const start_idx = result.positive_index;
                const new_clipped = try createClippedTriangle(model, cl.vertices, start_idx, plane, 
                                                              cl.normals, cl.uv_data, &normals, &uv_data, true);
                try clipped_triangles.append(new_clipped);
                addtl_clipped_count += 1;
            } else if (result.negative_count == 1) {
                const start_idx = result.negative_index;
                const two_clipped = try createTwoClippedTriangles(model, cl.vertices, start_idx, plane, 
                                                                  cl.normals, cl.uv_data, &normals, &uv_data, true);

                // zig fmt: on
                for (two_clipped) |new_clipped| {
                    try clipped_triangles.append(new_clipped);
                    addtl_clipped_count += 1;
                }
            }
        }

        while (addtl_clipped_count > 0) : (addtl_clipped_count -= 1) {
            try should_remove_triangle.append(false);
        }

        // Remove previously clipped triangles that have been additionally clipped
        var i: u32 = 0;
        while (i < should_remove_triangle.items.len) {
            if (should_remove_triangle.items[i]) {
                _ = clipped_triangles.swapRemove(i);
                _ = should_remove_triangle.swapRemove(i);
            } else {
                i += 1;
            }
        }

        should_remove_triangle.clearRetainingCapacity();

        // Find which fully visible triangles are clipped by this plane
        for (visible_triangles.items) |tri| {
            const v = [3]Vec3{
                transformed.items[tri.indices[0]],
                transformed.items[tri.indices[1]],
                transformed.items[tri.indices[2]],
            };

            const result = analyzeTriangleClipping(plane, v);

            if (result.positive_count != 3) {
                try should_remove_triangle.append(true);
            } else {
                try should_remove_triangle.append(false);
            }

            if (result.positive_count == 1) {
                // zig fmt: off
                const start_idx = result.positive_index;
                const clipped = try createClippedTriangle(model, v, start_idx, plane, tri.normals, 
                                                          tri.uv_data, &normals, &uv_data, false);
                try clipped_triangles.append(clipped);
            } else if (result.negative_count == 1) {
                const start_idx = result.negative_index;
                const two_clipped = try createTwoClippedTriangles(model, v, start_idx, plane, tri.normals, 
                                                                  tri.uv_data, &normals, &uv_data, false);
                // zig fmt: on
                for (two_clipped) |clipped| {
                    try clipped_triangles.append(clipped);
                }
            }
        }

        // Remove previously fully visible triangles that have been clipped
        i = 0;
        while (i < should_remove_triangle.items.len) {
            if (should_remove_triangle.items[i]) {
                _ = visible_triangles.swapRemove(i);
                _ = should_remove_triangle.swapRemove(i);
            } else {
                i += 1;
            }
        }

        should_remove_triangle.clearRetainingCapacity();
    }

    const rot_mat = vector.multiplyMM4(transposed_camera_rotation, obj.makeRotationMatrix());

    for (visible_triangles.items) |tri| {
        renderTriangle(screen, model, rot_mat, tri, transformed, texture, segment);
    }

    for (clipped_triangles.items) |clipped| {
        var clipped_normals: ?[3]Vec3 = null;
        var clipped_uvs: ?[3][2]f32 = null;
        if (clipped.normals != null) {
            clipped_normals = normals.items[clipped.normals.?];
        }

        if (clipped.uv_data != null) {
            clipped_uvs = uv_data.items[clipped.uv_data.?];
        }

        renderClippedTriangle(screen, rot_mat, clipped, clipped_normals, clipped_uvs, texture, segment);
    }
}

fn computeTriangleNormal(a: Vec3, b: Vec3, c: Vec3) Vec3 {
    const v1 = b.subtract(a);
    const v2 = c.subtract(a);

    return v1.cross(v2);
}

fn analyzeTriangleClipping(plane: vector.Plane, v: [3]Vec3) ClippingResult {
    var dist_is_positive = [3]bool{ false, false, false };

    dist_is_positive[0] = plane.normal.dot(v[0]) + plane.distance > 0;
    dist_is_positive[1] = plane.normal.dot(v[1]) + plane.distance > 0;
    dist_is_positive[2] = plane.normal.dot(v[2]) + plane.distance > 0;

    var positive_sum: u32 = 0;
    var negative_sum: u32 = 0;
    var positive_index: usize = 0;
    var negative_index: usize = 0;
    for (dist_is_positive) |positive, i| {
        if (positive) {
            positive_sum += 1;
            positive_index = i;
        } else {
            negative_sum += 1;
            negative_index = i;
        }
    }

    return .{
        .positive_count = positive_sum,
        .negative_count = negative_sum,
        .positive_index = positive_index,
        .negative_index = negative_index,
    };
}

// zig fmt: off
fn createClippedTriangle(model: Models.Model, v: [3]Vec3, start: usize, plane: vector.Plane, 
                         normals_idx: ?u16, uv_idx: ?u16, normals: *ArrayList([3]Vec3), 
                         uv_data: *ArrayList([3][2]f32), addtl_clip: bool) !Triangle {
    // zig fmt: on
    const a = v[start];
    const b = v[(start + 1) % 3];
    const c = v[(start + 2) % 3];

    const ab_intersect = plane.computeIntersectionFraction(a, b);
    const ac_intersect = plane.computeIntersectionFraction(a, c);

    const b_prime = a.add(b.subtract(a).multiply(ab_intersect));
    const c_prime = a.add(c.subtract(a).multiply(ac_intersect));

    const vertices = [3]Vec3{ a, b_prime, c_prime };

    var out_triangle = Triangle{
        .vertices = vertices,
        .normals = null,
        .uv_data = null,
    };

    if (normals_idx == null and uv_idx == null) {
        return out_triangle;
    }

    if (normals_idx != null) {
        var ns: [3]Vec3 = undefined;
        if (addtl_clip) {
            ns = normals.items[normals_idx.?];
        } else {
            ns = model.normals.items[normals_idx.?];
        }

        const na = ns[start];
        const nb = ns[(start + 1) % 3];
        const nc = ns[(start + 2) % 3];

        const nb_prime = na.add(nb.subtract(na).multiply(ab_intersect));
        const nc_prime = na.add(nc.subtract(na).multiply(ac_intersect));

        const new_normals = [3]Vec3{ na, nb_prime, nc_prime };

        try normals.append(new_normals);

        out_triangle.normals = @intCast(u16, normals.items.len - 1);
    }

    if (uv_idx != null) {
        var uvs: [3][2]f32 = undefined;
        if (addtl_clip) {
            uvs = uv_data.items[uv_idx.?];
        } else {
            uvs = model.uvs.items[uv_idx.?];
        }

        const uva = uvs[start];
        const uvb = uvs[(start + 1) % 3];
        const uvc = uvs[(start + 2) % 3];

        const uvb_prime = [2]f32{
            lerp.lerp(uva[0], uvb[0], ab_intersect),
            lerp.lerp(uva[1], uvb[1], ab_intersect),
        };

        //const uvc_prime = uva.add(uvc.subtract(uva).multiply(ac_intersect));
        const uvc_prime = [2]f32{
            lerp.lerp(uva[0], uvc[0], ac_intersect),
            lerp.lerp(uva[1], uvc[1], ac_intersect),
        };

        const new_uvs = [3][2]f32{ uva, uvb_prime, uvc_prime };

        try uv_data.append(new_uvs);

        out_triangle.uv_data = @intCast(u16, uv_data.items.len - 1);
    }

    return out_triangle;
}

// zig fmt: off
fn createTwoClippedTriangles(model: Models.Model, v: [3]Vec3, start: usize, plane: vector.Plane, 
                             normals_idx: ?u16, uv_idx: ?u16, normals: *ArrayList([3]Vec3), 
                             uv_data: *ArrayList([3][2]f32), addtl_clip: bool) ![2]Triangle {
    // zig fmt: on
    const c = v[start];
    const a = v[(start + 1) % 3];
    const b = v[(start + 2) % 3];

    const ac_intersect = plane.computeIntersectionFraction(a, c);
    const bc_intersect = plane.computeIntersectionFraction(b, c);

    const a_prime = a.add(c.subtract(a).multiply(ac_intersect));
    const b_prime = b.add(c.subtract(b).multiply(bc_intersect));

    const v1 = [3]Vec3{ a, b, a_prime };
    const v2 = [3]Vec3{ a_prime, b, b_prime };

    var triangle1 = Triangle{
        .vertices = v1,
        .normals = null,
        .uv_data = null,
    };

    var triangle2 = Triangle{
        .vertices = v2,
        .normals = null,
        .uv_data = null,
    };

    if (normals_idx == null and uv_idx == null) {
        return [2]Triangle{ triangle1, triangle2 };
    }

    if (normals_idx != null) {
        var ns: [3]Vec3 = undefined;
        if (addtl_clip) {
            ns = normals.items[normals_idx.?];
        } else {
            ns = model.normals.items[normals_idx.?];
        }

        const nc = ns[start];
        const na = ns[(start + 1) % 3];
        const nb = ns[(start + 2) % 3];

        const na_prime = na.add(nc.subtract(na).multiply(ac_intersect));
        const nb_prime = nb.add(nc.subtract(nb).multiply(bc_intersect));

        const new_ns1 = [3]Vec3{ na, nb, na_prime };
        const new_ns2 = [3]Vec3{ na_prime, nb, nb_prime };

        try normals.append(new_ns1);
        triangle1.normals = @intCast(u16, normals.items.len - 1);

        try normals.append(new_ns2);
        triangle2.normals = @intCast(u16, normals.items.len - 1);
    }

    if (uv_idx != null) {
        var uvs: [3][2]f32 = undefined;
        if (addtl_clip) {
            uvs = uv_data.items[uv_idx.?];
        } else {
            uvs = model.uvs.items[uv_idx.?];
        }

        const uvc = uvs[start];
        const uva = uvs[(start + 1) % 3];
        const uvb = uvs[(start + 2) % 3];

        const uva_prime = [2]f32{
            lerp.lerp(uva[0], uvc[0], ac_intersect),
            lerp.lerp(uva[1], uvc[1], ac_intersect),
        };
        const uvb_prime = [2]f32{
            lerp.lerp(uvb[0], uvc[0], bc_intersect),
            lerp.lerp(uvb[1], uvc[1], bc_intersect),
        };

        const new_uvs1 = [3][2]f32{ uva, uvb, uva_prime };
        const new_uvs2 = [3][2]f32{ uva_prime, uvb, uvb_prime };

        try uv_data.append(new_uvs1);
        triangle1.uv_data = @intCast(u16, uv_data.items.len - 1);

        try uv_data.append(new_uvs2);
        triangle2.uv_data = @intCast(u16, uv_data.items.len - 1);
    }

    return [2]Triangle{ triangle1, triangle2 };
}

// zig fmt: off
fn renderTriangle(screen: *Screen, model: Models.Model, rot_mat: [4][4]f32, tri: Models.Triangle, 
                  tran: ArrayList(Vec3), texture: ?Textures.Texture, segment: ScreenSegment) void {
    // zig fmt: on
    const vertices = [3]Vec3{
        tran.items[tri.indices[0]],
        tran.items[tri.indices[1]],
        tran.items[tri.indices[2]],
    };

    const tri_to_render = Triangle{
        .vertices = vertices,
        .normals = null,
        .uv_data = null,
    };

    var normals: ?[3]Vec3 = null;
    if (tri.normals != null) {
        normals = model.normals.items[tri.normals.?];
        for (normals.?) |*norm| {
            const new_norm = vector.multiplyMV4(rot_mat, norm.*.toHomogeneousVertex());
            norm.* = vector.homogeneousToVec(new_norm);
        }
    }

    var uv_data: ?[3][2]f32 = null;
    if (tri.uv_data != null) {
        uv_data = model.uvs.items[tri.uv_data.?];
    }

    drawFilledTriangle(screen, tri_to_render, normals, uv_data, texture, segment);
}

// zig fmt: off
fn renderClippedTriangle(screen: *Screen, rot_mat: [4][4]f32, cl: Triangle, 
                         cl_norm: ?[3]Vec3, cl_uv: ?[3][2]f32, texture: ?Textures.Texture, segment: ScreenSegment) void {
    // zig fmt: on
    var clipped_normals = cl_norm;

    if (clipped_normals != null) {
        for (clipped_normals.?) |*norm| {
            const new_norm = vector.multiplyMV4(rot_mat, norm.*.toHomogeneousVertex());
            norm.* = vector.homogeneousToVec(new_norm);
        }
    }

    drawFilledTriangle(screen, cl, clipped_normals, cl_uv, texture, segment);
}

fn putPixel(screen: *Screen, x: i32, y: i32, color: Color, inv_z: f32) void {
    const row = max_screen_y - y;
    const col = max_screen_x + x;
    if (row < 0 or row >= max_screen_y * 2) return;
    if (col < 0 or col >= max_screen_x * 2) return;

    const index = @intCast(u32, row) * screen_width + @intCast(u32, col);

    if (index >= screen.screen_buffer.len) return;

    const depth = screen.depth_buffer[index];

    if (depth == 0 or depth < inv_z) {
        screen.depth_buffer[index] = inv_z;

        screen.screen_buffer[index] = color.convertToSRGB().pack(screen.pixel_format);
    }
}

fn viewportToCanvasCoord(view_x: f32, view_y: f32) Vec2 {
    const canvas_x = view_x * @intToFloat(f32, screen_width) / viewport_width;
    const canvas_y = view_y * @intToFloat(f32, screen_height) / viewport_height;

    return .{
        .x = @floatToInt(i16, canvas_x),
        .y = @floatToInt(i16, canvas_y),
    };
}

fn projectVertex(v: Vec3) Vec2 {
    const view_x = v.x * projection_plane_d / v.z;
    const view_y = v.y * projection_plane_d / v.z;

    return viewportToCanvasCoord(view_x, view_y);
}

fn drawLine(screen: *Screen, pos1: Vec2, pos2: Vec2, color: Color) void {
    var x0 = pos1.x;
    var y0 = pos1.y;

    var x1 = pos2.x;
    var y1 = pos2.y;

    // Bresenham's line algorithm
    const dx: i16 = if (x1 - x0 >= 0) x1 - x0 else x0 - x1;
    const sx: i16 = if (x0 < x1) 1 else -1;
    const dy: i16 = if (y1 - y0 < 0) y1 - y0 else y0 - y1;
    const sy: i16 = if (y0 < y1) 1 else -1;
    var err = dx + dy;

    while (true) {
        putPixel(screen, x0, y0, color);

        if (x0 == x1 and y0 == y1) break;

        const err2 = 2 * err;

        if (err2 >= dy) {
            if (x0 == x1) break;
            err += dy;
            x0 += sx;
        }

        if (err2 <= dx) {
            if (y0 == y1) break;
            err += dx;
            y0 += sy;
        }
    }
}

fn drawWireTriangle(screen: *Screen, tri: Triangle) void {
    const p0 = projectVertex(tri.vertices[0]);
    const p1 = projectVertex(tri.vertices[1]);
    const p2 = projectVertex(tri.vertices[2]);

    drawLine(screen, p0, p1, tri.color);
    drawLine(screen, p1, p2, tri.color);
    drawLine(screen, p2, p0, tri.color);
}

// zig fmt: off
fn drawFilledTriangle(screen: *Screen, tri: Triangle, mdl_normals: ?[3]Vec3, 
                      tri_uv_data: ?[3][2]f32, texture: ?Textures.Texture, segment: ScreenSegment) void {
    // zig fmt: on
    var projected = [3]Vec2{
        projectVertex(tri.vertices[0]),
        projectVertex(tri.vertices[1]),
        projectVertex(tri.vertices[2]),
    };

    var unprojected = [3]Vec3{
        tri.vertices[0],
        tri.vertices[1],
        tri.vertices[2],
    };

    const normal = computeTriangleNormal(tri.vertices[0], tri.vertices[1], tri.vertices[2]);

    var n0 = normal;
    var n1 = normal;
    var n2 = normal;
    if (mdl_normals != null) {
        n0 = mdl_normals.?[0];
        n1 = mdl_normals.?[1];
        n2 = mdl_normals.?[2];
    }

    var sort_normals = [3]Vec3{ n0, n1, n2 };

    var uv_data = [3][2]f32{
        [2]f32{ 0, 0 },
        [2]f32{ 0, 0 },
        [2]f32{ 0, 0 },
    };

    if (tri_uv_data != null) {
        uv_data[0] = tri_uv_data.?[0];
        uv_data[1] = tri_uv_data.?[1];
        uv_data[2] = tri_uv_data.?[2];
    }

    sortProjectedVertices(&projected, &unprojected, &sort_normals, &uv_data);

    const color = .{ .r = 0, .g = 255, .b = 0 };

    const y0 = projected[0].y;
    const y1 = projected[1].y;
    const y2 = projected[2].y;

    const x0 = @intToFloat(f32, projected[0].x);
    const x1 = @intToFloat(f32, projected[1].x);
    const x2 = @intToFloat(f32, projected[2].x);

    const z0 = unprojected[0].z;
    const z1 = unprojected[1].z;
    const z2 = unprojected[2].z;

    const l0 = computeLighting(unprojected[0], sort_normals[0]);
    const l1 = computeLighting(unprojected[1], sort_normals[1]);
    const l2 = computeLighting(unprojected[2], sort_normals[2]);

    const x_values = lerp.edgeLerp(y0, y1, y2, x0, x1, x2);
    const inv_z_values = lerp.edgeLerp(y0, y1, y2, 1.0 / z0, 1.0 / z1, 1.0 / z2);
    const light_values = lerp.edgeLerp(y0, y1, y2, l0, l1, l2);

    var u_values: ?lerp.LerpEdges = null;
    var v_values: ?lerp.LerpEdges = null;

    var texture_to_use: ?Textures.Texture = null;
    if (tri_uv_data != null) {
        u_values = lerp.edgeLerp(y0, y1, y2, uv_data[0][0] / z0, uv_data[1][0] / z1, uv_data[2][0] / z2);
        v_values = lerp.edgeLerp(y0, y1, y2, uv_data[0][1] / z0, uv_data[1][1] / z1, uv_data[2][1] / z2);

        texture_to_use = texture;
    }

    // Used to determine which side is left and which is right
    const middle: i32 = x_values.edge1.getMiddle();
    const edge1_middle = x_values.edge1.interpolateAt(middle);
    const edge2_middle = x_values.edge2.interpolateAt(middle);

    // Fill in horizontal rows between left and right sides
    if (edge1_middle < edge2_middle) {
        const x_left = x_values.edge1;
        const x_right = x_values.edge2;
        const z_left = inv_z_values.edge1;
        const z_right = inv_z_values.edge2;
        const light_left = light_values.edge1;
        const light_right = light_values.edge2;

        var u_left: ?lerp.LerpEdge = null;
        var u_right: ?lerp.LerpEdge2 = null;
        var v_left: ?lerp.LerpEdge = null;
        var v_right: ?lerp.LerpEdge2 = null;

        if (tri_uv_data != null) {
            u_left = u_values.?.edge1;
            u_right = u_values.?.edge2;
            v_left = v_values.?.edge1;
            v_right = v_values.?.edge2;
        }

        // zig fmt: off
        shadeTriangle(screen, x_left, x_right, z_left, z_right, light_left, light_right, 
                      u_left, u_right, v_left, v_right, y0, y2, color, texture_to_use, segment);
        // zig fmt: on
    } else {
        const x_left = x_values.edge2;
        const x_right = x_values.edge1;
        const z_left = inv_z_values.edge2;
        const z_right = inv_z_values.edge1;
        const light_left = light_values.edge2;
        const light_right = light_values.edge1;

        var u_left: ?lerp.LerpEdge2 = null;
        var u_right: ?lerp.LerpEdge = null;
        var v_left: ?lerp.LerpEdge2 = null;
        var v_right: ?lerp.LerpEdge = null;

        if (tri_uv_data != null) {
            u_left = u_values.?.edge2;
            u_right = u_values.?.edge1;
            v_left = v_values.?.edge2;
            v_right = v_values.?.edge1;
        }

        // zig fmt: off
        shadeTriangle(screen, x_left, x_right, z_left, z_right, light_left, light_right, 
                      u_left, u_right, v_left, v_right, y0, y2, color, texture_to_use, segment);
        // zig fmt: on
    }
}

fn sortProjectedVertices(projected: *[3]Vec2, unprojected: *[3]Vec3, normals: *[3]Vec3, uv_data: *[3][2]f32) void {
    if (projected[1].y < projected[0].y) {
        const temp = projected[0];
        projected[0] = projected[1];
        projected[1] = temp;

        const temp2 = unprojected[0];
        unprojected[0] = unprojected[1];
        unprojected[1] = temp2;

        const temp3 = normals[0];
        normals[0] = normals[1];
        normals[1] = temp3;

        const temp4 = uv_data[0];
        uv_data[0] = uv_data[1];
        uv_data[1] = temp4;
    }
    if (projected[2].y < projected[0].y) {
        const temp = projected[0];
        projected[0] = projected[2];
        projected[2] = temp;

        const temp2 = unprojected[0];
        unprojected[0] = unprojected[2];
        unprojected[2] = temp2;

        const temp3 = normals[0];
        normals[0] = normals[2];
        normals[2] = temp3;

        const temp4 = uv_data[0];
        uv_data[0] = uv_data[2];
        uv_data[2] = temp4;
    }
    if (projected[2].y < projected[1].y) {
        const temp = projected[1];
        projected[1] = projected[2];
        projected[2] = temp;

        const temp2 = unprojected[1];
        unprojected[1] = unprojected[2];
        unprojected[2] = temp2;

        const temp3 = normals[1];
        normals[1] = normals[2];
        normals[2] = temp3;

        const temp4 = uv_data[1];
        uv_data[1] = uv_data[2];
        uv_data[2] = temp4;
    }
}

// zig fmt: off
fn shadeTriangle(screen: *Screen, x_left: anytype, x_right: anytype, z_left: anytype, 
                 z_right: anytype, l_left: anytype, l_right: anytype, u_left: anytype, 
                 u_right: anytype, v_left: anytype, v_right: anytype, min_y: i32, max_y: i32, 
                 color: Color, texture: ?Textures.Texture, segment: ScreenSegment) void {
    // zig fmt: on
    var adjusted_max_y = if (max_y <= segment.max_y) max_y else segment.max_y;
    var y = if (min_y >= segment.min_y) min_y else segment.min_y;
    while (y <= adjusted_max_y) : (y += 1) {
        const y_step = y - min_y;
        const min_x_f = x_left.interpolateAt(y_step);
        const max_x_f = x_right.interpolateAt(y_step);

        const min_x = @floatToInt(i32, min_x_f);
        const max_x = @floatToInt(i32, max_x_f);

        const z_left_at_current_y = z_left.interpolateAt(y_step);
        const z_right_at_current_y = z_right.interpolateAt(y_step);

        const l_left_at_current_y = l_left.interpolateAt(y_step);
        const l_right_at_current_y = l_right.interpolateAt(y_step);

        var u_left_at_current_y: ?f32 = null;
        var u_right_at_current_y: ?f32 = null;
        var v_left_at_current_y: ?f32 = null;
        var v_right_at_current_y: ?f32 = null;

        if (texture != null) {
            u_left_at_current_y = u_left.?.interpolateAt(y_step);
            u_right_at_current_y = u_right.?.interpolateAt(y_step);
            v_left_at_current_y = v_left.?.interpolateAt(y_step);
            v_right_at_current_y = v_right.?.interpolateAt(y_step);
        }

        var adjusted_max_x = if (max_x <= segment.max_x) max_x else segment.max_x;
        var x = if (min_x >= segment.min_x) min_x else segment.min_x;
        while (x <= adjusted_max_x) : (x += 1) {
            const t = lerp.getPercentage(@intToFloat(f32, x), min_x_f, max_x_f);
            const inv_z_value = lerp.lerp(z_left_at_current_y, z_right_at_current_y, t);
            const light_value = lerp.lerp(l_left_at_current_y, l_right_at_current_y, t);
            var color_to_put = color;

            if (texture != null) {
                const u_value = lerp.lerp(u_left_at_current_y.?, u_right_at_current_y.?, t);
                const v_value = lerp.lerp(v_left_at_current_y.?, v_right_at_current_y.?, t);

                color_to_put = getTexelColor(u_value / inv_z_value, v_value / inv_z_value, texture.?);
            }

            putPixel(screen, x, y, color_to_put.multiplyFloat(light_value), inv_z_value);
        }
    }
}

fn getTexelColor(u: f32, v: f32, texture: Textures.Texture) Color {
    var x_f = u * @intToFloat(f32, texture.width);
    var y_f = v * @intToFloat(f32, texture.height);

    if (x_f < 0) x_f = 0;
    if (y_f < 0) y_f = 0;

    const tx = @floatToInt(u32, x_f);
    const ty = @floatToInt(u32, y_f);
    const frac_x = std.math.modf(x_f).fpart;
    const frac_y = std.math.modf(y_f).fpart;

    var indices = [4]usize{
        ty * texture.width + tx,
        ty * texture.width + (tx + 1),
        (ty + 1) * texture.width + tx,
        (ty + 1) * texture.width + (tx + 1),
    };

    for (indices) |*idx, i| {
        if (idx.* >= texture.texels.items.len) {
            if (i == 0) {
                const diff = idx.* - texture.texels.items.len;

                const new_index = idx.* - diff - 1;

                idx.* = new_index;
            } else {
                idx.* = indices[0];
            }
        }
    }

    const top_left = texture.texels.items[indices[0]];

    // Hack to prevent incorrect filtering on right edge of texture
    if ((indices[0] + 1) % texture.width == 0) return top_left.color;

    const top_right = texture.texels.items[indices[1]];
    const bottom_left = texture.texels.items[indices[2]];
    const bottom_right = texture.texels.items[indices[3]];

    const top_center = top_right.color
        .multiplyFloat(frac_x)
        .add(top_left.color.multiplyFloat(1 - frac_x));

    const bottom_center = bottom_right.color
        .multiplyFloat(frac_x)
        .add(bottom_left.color.multiplyFloat(1 - frac_x));

    return bottom_center
        .multiplyFloat(frac_y)
        .add(top_center.multiplyFloat(1 - frac_y));
}

fn computeLighting(vertex: Vec3, normal: Vec3) f32 {
    var lighting: f32 = 0;

    for (lights) |light| {
        if (light.tag == .ambient) {
            lighting += light.intensity;
            continue;
        }

        var light_vec: Vec3 = undefined;

        if (light.tag == .directional) {
            const rotated = vector.multiplyMV4(transposed_camera_rotation, light.vector.toHomogeneousVertex());
            light_vec = vector.homogeneousToVec(rotated);
        } else if (light.tag == .point) {
            const transformed = vector.multiplyMV4(camera_matrix, light.vector.toHomogeneousVertex());
            const transformed_vec = vector.homogeneousToVec(transformed);

            light_vec = transformed_vec.subtract(vertex);
        }

        const angle_cos = light_vec.dot(normal) / (light_vec.length() * normal.length());
        if (angle_cos > 0) {
            lighting += angle_cos * light.intensity;
        }
    }

    return lighting;
}

fn processInput() void {
    if (Input.isButtonPressed(.right)) {
        camera_y_angle += 0.05;
    }

    if (Input.isButtonPressed(.left)) {
        camera_y_angle -= 0.05;
    }

    if (Input.isButtonPressed(.up)) {
        camera_x_angle -= 0.05;
    }
    if (Input.isButtonPressed(.down)) {
        camera_x_angle += 0.05;
    }
    if (Input.isButtonPressed(.w)) {
        moveCamera(0.2, false);
    }
    if (Input.isButtonPressed(.a)) {
        moveCamera(-0.2, true);
    }
    if (Input.isButtonPressed(.s)) {
        moveCamera(-0.2, false);
    }
    if (Input.isButtonPressed(.d)) {
        moveCamera(0.2, true);
    }
}

fn moveCamera(increment: f32, strafe: bool) void {
    var unit_vector = Vec3{ .x = 0, .y = 0, .z = 1 };

    if (strafe) {
        unit_vector.z = 0;
        unit_vector.x = 1;
    }

    const y_rotation = vector.createQuaternion(.{ .x = 0, .y = 1, .z = 0 }, camera_y_angle);
    const camera_look_dir = y_rotation
        .rotateVector(unit_vector)
        .normalize();

    camera_position = camera_position.add(camera_look_dir.multiply(increment));
}

fn clearScreen(screen: *Screen) void {
    for (screen.screen_buffer) |*pixel, i| {
        pixel.* = 0;
        screen.depth_buffer[i] = 0;
    }
}

fn drawFrameTimer(screen: *Screen, dt: f32) void {
    const numbers = Textures.textures.get(.numbers) orelse return;
    _ = screen;

    const modf_result = std.math.modf(dt);

    const int_part = @floatToInt(u16, modf_result.ipart);
    const frac_part = @floatToInt(u16, (modf_result.fpart + 0.005) * 100);

    const tens = int_part / 10;
    const ones = int_part - (tens * 10);
    const tenths = frac_part / 10;
    const hundredths = frac_part - (tenths * 10);

    const numbers_to_draw = [5]u16{ tens, ones, 10, tenths, hundredths };

    var screen_col_offset: u32 = 0;
    for (numbers_to_draw) |num| {
        var row: u32 = 0;
        while (row < numbers.height) : (row += 1) {
            // assuming individual number width = numbers texture height
            const start = num * numbers.height;
            var col: u32 = start;
            while (col < start + numbers.height) : (col += 1) {
                const screen_col = col - start + screen_col_offset;
                const screen_pixel = screen.screen_buffer[row * screen_width + screen_col];
                const screen_color = colors
                    .fromScreenValue(screen_pixel, screen.pixel_format)
                    .convertToLinear();
                const texel = numbers.texels.items[row * numbers.width + col];

                const alpha = @intToFloat(f32, texel.alpha) / 255.0;

                const result_color = texel.color
                    .add(screen_color.multiplyFloat(1 - alpha));

                screen.screen_buffer[row * screen_width + screen_col] = result_color
                    .convertToSRGB()
                    .pack(screen.pixel_format);
            }
        }

        screen_col_offset += numbers.height;
    }
}
