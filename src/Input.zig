pub const MousePosition = struct { x: i32, y: i32 };

pub const InputValue = enum(u32) {
    w = 1,
    a = 2,
    s = 4,
    d = 8,
    left = 16,
    right = 32,
    up = 64,
    down = 128,
};

var last_frame_input: u32 = 0;
var input: u32 = 0;

pub fn pressButton(button: InputValue) void {
    input |= @enumToInt(button);
}
pub fn releaseButton(button: InputValue) void {
    input &= ~(@enumToInt(button));
}

pub fn resetButtons() void {
    input = 0;
}

pub fn isButtonPressed(button: InputValue) bool {
    return (input & @enumToInt(button)) > 0;
}

pub fn isButtonPressedThisFrame(button: InputValue) bool {
    const button_pressed: bool = (input & @enumToInt(button)) > 0;
    const last_frame_pressed = (last_frame_input & @enumToInt(button)) > 0;

    return button_pressed and !last_frame_pressed;
}

pub fn hasInputChangedThisFrame() bool {
    return input != last_frame_input;
}

pub fn isButtonReleased(button: InputValue) bool {
    return (input & @enumToInt(button)) == 0;
}

pub fn saveFrameInput() void {
    last_frame_input = input;
}
