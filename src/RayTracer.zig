const std = @import("std");
const vector = @import("vector_math.zig");
const Input = @import("Input.zig");
const colors = @import("colors.zig");

const infinity = std.math.inf_f32;
const sqrt = std.math.sqrt;
const pow = std.math.pow;

const frame_allocator = std.heap.c_allocator;

const EPSILON = 0.01;

const Vec3 = vector.Vec3;
const Quaternion = vector.Quaternion;
const Color = colors.Color;
const PixelFormat = colors.PixelFormat;

const Sphere = struct {
    center: Vec3,
    radius: f32,
    color: Color,
    specularity: f32,
    reflectivity: f32,
};

const SphereIntersect = struct {
    index: ?usize,
    closest_t: f32,
};

const LightType = enum { ambient, point, directional };

const Light = struct {
    tag: LightType,
    intensity: f32,
    position: Vec3,
};

// width = 2 * tan(fov / 2) * distance
const viewport_width: f32 = 1.0;
const viewport_height: f32 = 1.0;
const projection_plane_d: f32 = 1.0;

var screen_width: u32 = 0;
var screen_height: u32 = 0;
var max_screen_x: i32 = 0;
var max_screen_y: i32 = 0;
var spheres: [4]Sphere = undefined;
var lights: [3]Light = undefined;
var camera_position: Vec3 = .{ .x = 0, .y = 0, .z = 0 };
var camera_y_angle: f32 = 0.0;
var camera_x_angle: f32 = 0.0;

pub fn initialize(width: u32, height: u32) void {
    screen_width = width;
    screen_height = height;
    max_screen_x = @intCast(i32, width / 2);
    max_screen_y = @intCast(i32, height / 2);

    const sphere1 = .{
        .center = .{
            .x = 0,
            .y = -1,
            .z = 3,
        },
        .radius = 1,
        .color = .{ .r = 255, .g = 0, .b = 0 },
        .specularity = 500,
        .reflectivity = 0.2,
    };
    const sphere2 = .{
        .center = .{
            .x = 2,
            .y = 0,
            .z = 4,
        },
        .radius = 1,
        .color = .{ .r = 0, .g = 0, .b = 255 },
        .specularity = 500,
        .reflectivity = 0.3,
    };
    const sphere3 = .{
        .center = .{
            .x = -2,
            .y = 0,
            .z = 4,
        },
        .radius = 1,
        .color = .{ .r = 0, .g = 255, .b = 0 },
        .specularity = 10,
        .reflectivity = 0.4,
    };
    const sphere4 = .{
        .center = .{
            .x = 0,
            .y = -5001,
            .z = 0,
        },
        .radius = 5000,
        .color = .{ .r = 255, .g = 255, .b = 0 },
        .specularity = 1000,
        .reflectivity = 0.0,
    };

    spheres[0] = sphere1;
    spheres[1] = sphere2;
    spheres[2] = sphere3;
    spheres[3] = sphere4;

    const light1 = .{
        .tag = .ambient,
        .intensity = 0.2,
        .position = .{ .x = 0, .y = 0, .z = 0 },
    };

    const light2 = .{
        .tag = .point,
        .intensity = 0.6,
        .position = .{ .x = 2, .y = 1, .z = 0 },
    };

    const light3 = .{
        .tag = .directional,
        .intensity = 0.2,
        .position = .{ .x = 1, .y = 4, .z = 4 },
    };

    lights[0] = light1;
    lights[1] = light2;
    lights[2] = light3;
}

pub fn renderScene(screen_buffer: []u32, pixel_format: PixelFormat) !void {
    handleInput();

    const camera_rotation_y = vector.createQuaternion(.{ .x = 0, .y = 1, .z = 0 }, camera_y_angle);
    const camera_rotation_x = vector.createQuaternion(.{ .x = 1, .y = 0, .z = 0 }, camera_x_angle);
    const camera_rotation = camera_rotation_y.hamiltonProduct(camera_rotation_x).normalize();

    var frame1 = try frame_allocator.create(@Frame(drawScreen));
    defer frame_allocator.destroy(frame1);
    var frame2 = try frame_allocator.create(@Frame(drawScreen));
    defer frame_allocator.destroy(frame2);
    var frame3 = try frame_allocator.create(@Frame(drawScreen));
    defer frame_allocator.destroy(frame3);
    var frame4 = try frame_allocator.create(@Frame(drawScreen));
    defer frame_allocator.destroy(frame4);

    frame1.* = async drawScreen(screen_buffer, -max_screen_x, max_screen_y, 0, 0, camera_rotation, pixel_format);
    frame2.* = async drawScreen(screen_buffer, 0, max_screen_y, max_screen_x, 0, camera_rotation, pixel_format);
    frame3.* = async drawScreen(screen_buffer, -max_screen_x, 0, 0, -max_screen_y, camera_rotation, pixel_format);
    frame4.* = async drawScreen(screen_buffer, 0, 0, max_screen_x, -max_screen_y, camera_rotation, pixel_format);

    _ = await frame1;
    _ = await frame2;
    _ = await frame3;
    _ = await frame4;
}

fn drawScreen(buffer: []u32, x1: i32, y1: i32, x2: i32, y2: i32, cam_rot: Quaternion, format: PixelFormat) void {
    std.event.Loop.startCpuBoundOperation();

    var x: i32 = x1;
    while (x < x2) : (x += 1) {
        var y: i32 = y1;
        while (y > y2) : (y -= 1) {
            const direction = canvasToViewportCoord(@intToFloat(f32, x), @intToFloat(f32, y));
            const rotated_direction = cam_rot
                .hamiltonProduct(.{ .v = direction, .w = 0 })
                .hamiltonProduct(cam_rot.conjugate());
            const color = traceRay(camera_position, rotated_direction.v, 1, infinity, 2);
            putPixel(buffer, x, y, color, format);
        }
    }
}

fn putPixel(screen_buffer: []u32, x: i32, y: i32, color: Color, pixel_format: PixelFormat) void {
    const row = max_screen_y - y;
    const col = max_screen_x + x;
    const index = @intCast(u32, row) * screen_width + @intCast(u32, col);

    if (index >= screen_buffer.len or index < 0) return;

    screen_buffer[index] = color.pack(pixel_format);
}

fn canvasToViewportCoord(canvas_x: f32, canvas_y: f32) Vec3 {
    const view_x = canvas_x * viewport_width / @intToFloat(f32, screen_width);
    const view_y = canvas_y * viewport_height / @intToFloat(f32, screen_height);

    return .{
        .x = view_x,
        .y = view_y,
        .z = projection_plane_d,
    };
}

fn intersectRaySphere(origin: Vec3, direction: Vec3, sphere: Sphere) [2]f32 {
    const origin_to_center = origin.subtract(sphere.center);

    const a = direction.dot(direction);
    const b = 2 * origin_to_center.dot(direction);
    const c = origin_to_center.dot(origin_to_center) - sphere.radius * sphere.radius;

    // Finding intersections with sphere means solving quadratic formula with above terms
    const discriminant = b * b - 4 * a * c;

    if (discriminant < 0) {
        return [2]f32{ infinity, infinity };
    }

    const t1 = (-b + sqrt(discriminant)) / (2.0 * a);
    const t2 = (-b - sqrt(discriminant)) / (2.0 * a);

    return [2]f32{ t1, t2 };
}

fn traceRay(origin: Vec3, direction: Vec3, min_t: f32, max_t: f32, depth: u32) Color {
    const intersection = findClosestIntersection(origin, direction, min_t, max_t);
    const closest_sphere = intersection.index;
    const closest_t = intersection.closest_t;

    if (closest_sphere == null) {
        return .{ .r = 0, .g = 0, .b = 0 };
    }

    const sphere = spheres[closest_sphere.?];

    const point = origin.add(direction.multiply(closest_t));
    var normal = point.subtract(sphere.center);
    normal = normal.multiply(1.0 / normal.length());

    const view = direction.multiply(-1);
    const light_intensity = computeLighting(point, normal, view, sphere.specularity);
    const base_color = sphere.color.multiplyFloat(light_intensity);

    const r = sphere.reflectivity;

    if (r <= 0 or depth <= 0) {
        return base_color;
    }

    const reflection_dir = reflectRay(view, normal);
    const reflected_color = traceRay(point, reflection_dir, EPSILON, infinity, depth - 1);

    return lerpColor(base_color, reflected_color, r);
}

fn lerpColor(color1: Color, color2: Color, coeff: f32) Color {
    const first_term = color1.multiplyFloat(1.0 - coeff);
    const second_term = color2.multiplyFloat(coeff);

    return first_term.add(second_term);
}

fn findClosestIntersection(origin: Vec3, direction: Vec3, min_t: f32, max_t: f32) SphereIntersect {
    var closest_t: f32 = infinity;
    var closest_sphere: ?usize = null;

    for (spheres) |sphere, i| {
        const ts = intersectRaySphere(origin, direction, sphere);

        if (ts[0] < closest_t and min_t < ts[0] and ts[0] < max_t) {
            closest_t = ts[0];
            closest_sphere = i;
        }

        if (ts[1] < closest_t and min_t < ts[1] and ts[1] < max_t) {
            closest_t = ts[1];
            closest_sphere = i;
        }
    }

    return .{ .index = closest_sphere, .closest_t = closest_t };
}

fn findAnyIntersection(origin: Vec3, direction: Vec3, min_t: f32, max_t: f32) bool {
    var intersection_found = false;

    for (spheres) |sphere| {
        const ts = intersectRaySphere(origin, direction, sphere);

        if (ts[0] < infinity and min_t < ts[0] and ts[0] < max_t) {
            intersection_found = true;
            break;
        }

        if (ts[1] < infinity and min_t < ts[1] and ts[1] < max_t) {
            intersection_found = true;
            break;
        }
    }

    return intersection_found;
}

fn computeLighting(point: Vec3, normal: Vec3, vec_v: Vec3, specularity: f32) f32 {
    var intensity: f32 = 0.0;

    for (lights) |light| {
        if (light.tag == .ambient) {
            intensity += light.intensity;
        } else {
            const vec_l = if (light.tag == .point) light.position.subtract(point) else light.position;
            const max_t = if (light.tag == .point) 1 else infinity;

            const n_dot_l = normal.dot(vec_l);

            // For shadows: skip lighting if there's an object between point and light source
            const intersection_found = findAnyIntersection(point, vec_l, EPSILON, max_t);
            if (intersection_found) {
                continue;
            }

            // Diffuse reflections
            if (n_dot_l > 0) {
                intensity += light.intensity * n_dot_l / vec_l.length();
            }

            // Specular reflections
            if (specularity >= 0) {
                const vec_r = reflectRay(vec_l, normal);

                const r_dot_v = vec_r.dot(vec_v);
                const length_r = vec_r.length();
                const length_v = vec_v.length();

                if (r_dot_v > 0) {
                    intensity += light.intensity * pow(f32, r_dot_v / (length_r * length_v), specularity);
                }
            }
        }
    }

    return intensity;
}

fn reflectRay(vec: Vec3, normal: Vec3) Vec3 {
    return normal
        .multiply(2)
        .multiply(normal.dot(vec))
        .subtract(vec);
}

fn handleInput() void {
    const inc = 0.1;
    const rot_inc = 0.1;

    if (Input.isButtonPressed(.w)) {
        camera_position.z += inc;
    }

    if (Input.isButtonPressed(.a)) {
        camera_position.x -= inc;
    }

    if (Input.isButtonPressed(.s)) {
        camera_position.z -= inc;
    }

    if (Input.isButtonPressed(.d)) {
        camera_position.x += inc;
    }

    if (Input.isButtonPressed(.left)) {
        camera_y_angle -= rot_inc;
    }

    if (Input.isButtonPressed(.right)) {
        camera_y_angle += rot_inc;
    }

    if (Input.isButtonPressed(.up)) {
        camera_x_angle -= rot_inc;
    }

    if (Input.isButtonPressed(.down)) {
        camera_x_angle += rot_inc;
    }
}
