const c = @cImport({
    @cInclude("SDL.h");
});

const std = @import("std");
const constants = @import("constants.zig");
//const RayTracer = @import("RayTracer.zig");
const Rasterizer = @import("Rasterizer.zig");
const Input = @import("Input.zig");
const colors = @import("colors.zig");

var allocator = std.heap.page_allocator;

const screen_buffer_size = constants.WIN_WIDTH * constants.WIN_HEIGHT;
//const target_frame_time = 16666667;
const target_frame_time = 33333333;
//const target_frame_time = 50000000;

pub const io_mode = .evented;

pub fn main() !void {
    if (c.SDL_Init(c.SDL_INIT_VIDEO) != 0) {
        c.SDL_Log("Failed to initialize video: %s", c.SDL_GetError());

        return error.SDLInitializationFailed;
    }
    defer c.SDL_Quit();

    // zig fmt: off
    const window =
        c.SDL_CreateWindow("3D Soft Renderer", 10, 10, 
                           constants.WIN_WIDTH, constants.WIN_HEIGHT, c.SDL_WINDOW_RESIZABLE) orelse {
        c.SDL_Log("Failed to create window: %s", c.SDL_GetError());
        return error.SDLInitializationFailed;
    };
    defer c.SDL_DestroyWindow(window);

    const renderer = c.SDL_CreateRenderer(window, -1, 0) orelse {
        c.SDL_Log("Failed to create renderer: %s", c.SDL_GetError());
        return error.SDLInitializationFailed;
    };

    var screen_texture = c.SDL_CreateTexture(renderer, c.SDL_PIXELFORMAT_ARGB8888, 
                                             c.SDL_TEXTUREACCESS_STREAMING, 
                                             constants.WIN_WIDTH, constants.WIN_HEIGHT) orelse {
        c.SDL_Log("Failed to create screen texture: %s", c.SDL_GetError());
        return error.SDLInitializationFailed;
    };
    // zig fmt: on

    var screen_buffer = try allocator.alloc(u32, screen_buffer_size);
    var screen_buffer_slice = screen_buffer[0..screen_buffer_size];
    var depth_buffer = try allocator.alloc(f32, screen_buffer_size);
    defer allocator.free(screen_buffer);
    defer allocator.free(depth_buffer);

    for (depth_buffer) |*depth| {
        depth.* = 0;
    }

    for (screen_buffer) |*scr| {
        scr.* = 0;
    }

    var screen = .{
        .screen_buffer = screen_buffer_slice,
        .depth_buffer = depth_buffer,
        .pixel_format = colors.PixelFormat.rgb888,
    };

    try Rasterizer.initialize(constants.WIN_WIDTH, constants.WIN_HEIGHT);

    var start_time = std.time.nanoTimestamp();
    var dt: f32 = 16.0;

    var should_quit = false;
    while (!should_quit) {
        var event: c.SDL_Event = undefined;
        while (c.SDL_PollEvent(&event) != 0) {
            switch (event.@"type") {
                c.SDL_QUIT => {
                    should_quit = true;
                },
                else => {},
            }
        }

        const keys = c.SDL_GetKeyboardState(null);
        handleKeyState(keys);

        try Rasterizer.renderScene(&screen, dt);
        _ = c.SDL_UpdateTexture(screen_texture, 0, screen.screen_buffer, constants.WIN_WIDTH * 4);
        _ = c.SDL_RenderCopy(renderer, screen_texture, 0, 0);
        _ = c.SDL_RenderPresent(renderer);

        Input.saveFrameInput();

        var end_time = std.time.nanoTimestamp();
        const delta = end_time - start_time;
        const sleeptime = target_frame_time - delta;

        dt = @intToFloat(f32, delta) / 1000000.0;

        if (sleeptime > 0) std.time.sleep(@intCast(u64, sleeptime));

        start_time = std.time.nanoTimestamp();
    }
}

fn getPixelFormat(sdl_format: u32) !colors.PixelFormat {
    switch (sdl_format) {
        c.SDL_PIXELFORMAT_RGB888 => return colors.PixelFormat.rgb888,
        else => return error.UnsupportedPixelFormat,
    }
}

fn handleKeyState(keys: [*]const u8) void {
    Input.resetButtons();

    if (keys[c.SDL_SCANCODE_W] > 0) Input.pressButton(.w);
    if (keys[c.SDL_SCANCODE_A] > 0) Input.pressButton(.a);
    if (keys[c.SDL_SCANCODE_S] > 0) Input.pressButton(.s);
    if (keys[c.SDL_SCANCODE_D] > 0) Input.pressButton(.d);
    if (keys[c.SDL_SCANCODE_LEFT] > 0) Input.pressButton(.left);
    if (keys[c.SDL_SCANCODE_RIGHT] > 0) Input.pressButton(.right);
    if (keys[c.SDL_SCANCODE_UP] > 0) Input.pressButton(.up);
    if (keys[c.SDL_SCANCODE_DOWN] > 0) Input.pressButton(.down);
}
