const std = @import("std");
const Models = @import("Models.zig");
const vector = @import("vector_math.zig");

const Vec3 = vector.Vec3;
const Normals = std.ArrayList(Vec3);
const UVs = std.ArrayList([2]f32);

const State = enum {
    start,
    vertex,
    normal,
    uv,
    face,
};

const keywords = std.ComptimeStringMap(State, .{
    .{ "v", .vertex },
    .{ "vt", .uv },
    .{ "vn", .normal },
    .{ "f", .face },
});

const Face = struct {
    indices: [3]usize,
    normals: [3]Vec3,
    uvs: [3][2]f32,
};

pub fn parseLine(line: []const u8, model: *Models.Model, normals: *Normals, uvs: *UVs) !void {
    var state: State = .start;

    var start_idx: usize = eatWhitespace(line, 0);

    while (grabWord(line, start_idx)) |w| {
        switch (state) {
            .start => {
                if (keywords.get(w)) |st| {
                    state = st;
                } else {
                    break;
                }
            },
            .vertex => {
                const vec = try parseVector(line, start_idx);
                try model.vertices.append(vec);
                break;
            },
            .normal => {
                const vec = try parseVector(line, start_idx);
                try normals.append(vec);
                break;
            },
            .uv => {
                const uv = try parseUV(line, start_idx);
                try uvs.append(uv);
                break;
            },
            .face => {
                const face = try parseFace(line, start_idx, normals, uvs);

                const indices = [3]u16{
                    std.math.cast(u16, face.indices[0]) orelse return error.OutOfRange,
                    std.math.cast(u16, face.indices[1]) orelse return error.OutOfRange,
                    std.math.cast(u16, face.indices[2]) orelse return error.OutOfRange,
                };

                try model.normals.append(face.normals);
                try model.uvs.append(face.uvs);

                const tri = Models.Triangle{
                    .indices = indices,
                    .normals = std.math.cast(u16, model.normals.items.len - 1) orelse return error.OutOfRange,
                    .uv_data = std.math.cast(u16, model.uvs.items.len - 1) orelse return error.OutOfRange,
                };

                try model.triangles.append(tri);
                break;
            },
        }

        start_idx = eatWhitespace(line, start_idx + w.len);
    }
}

fn eatWhitespace(line: []const u8, start_idx: usize) usize {
    var index = start_idx;
    while (index < line.len) {
        const c = line[index];
        switch (c) {
            ' ', '\n', '\t', '\r' => {
                index += 1;
            },
            else => {
                break;
            },
        }
    }

    return index;
}
fn grabWord(line: []const u8, start_idx: usize) ?[]const u8 {
    if (start_idx >= line.len) return null;

    var end = start_idx;
    while (end < line.len) {
        const c = line[end];
        switch (c) {
            ' ', '\n', '\t', '\r' => {
                break;
            },
            else => {
                end += 1;
            },
        }
    }

    return line[start_idx..end];
}

fn parseVector(line: []const u8, start_idx: usize) !Vec3 {
    var index = start_idx;

    const v1 = grabWord(line, index) orelse return error.ExpectedVertex;
    index = eatWhitespace(line, index + v1.len);

    const v2 = grabWord(line, index) orelse return error.ExpectedVertex;
    index = eatWhitespace(line, index + v2.len);

    const v3 = grabWord(line, index) orelse return error.ExpectedVertex;
    index = eatWhitespace(line, index + v3.len);

    const v1_f = try parseFloat(v1);
    const v2_f = try parseFloat(v2);
    const v3_f = try parseFloat(v3);

    return Vec3{
        .x = v1_f,
        .y = v2_f,
        .z = v3_f,
    };
}

fn parseUV(line: []const u8, start_idx: usize) ![2]f32 {
    var index = start_idx;

    const v1 = grabWord(line, index) orelse return error.ExpectedVertex;
    index = eatWhitespace(line, index + v1.len);

    const v2 = grabWord(line, index) orelse return error.ExpectedVertex;
    index = eatWhitespace(line, index + v2.len);

    const v1_f = try parseFloat(v1);
    const v2_f = try parseFloat(v2);

    return [2]f32{ v1_f, v2_f };
}

fn parseFace(line: []const u8, start_idx: usize, normals: *Normals, uvs: *UVs) !Face {
    const FaceState = enum {
        vertex_idx,
        normal_idx,
        uv_idx,
    };

    var vertex_indices = [3]usize{ 0, 0, 0 };
    var tri_normals = [3]Vec3{
        .{ .x = 0, .y = 0, .z = 0 },
        .{ .x = 0, .y = 0, .z = 0 },
        .{ .x = 0, .y = 0, .z = 0 },
    };
    var tri_uvs = [3][2]f32{
        [2]f32{ 0, 0 },
        [2]f32{ 0, 0 },
        [2]f32{ 0, 0 },
    };

    var index = start_idx;

    const idx1 = grabWord(line, index) orelse return error.ExpectedIndicies;
    index = eatWhitespace(line, index + idx1.len);

    const idx2 = grabWord(line, index) orelse return error.ExpectedIndices;
    index = eatWhitespace(line, index + idx2.len);

    const idx3 = grabWord(line, index) orelse return error.ExpectedIndices;
    index = eatWhitespace(line, index + idx3.len);

    const to_parse = [3][]const u8{ idx1, idx2, idx3 };

    var state: FaceState = .vertex_idx;
    var idx_to_put: usize = 0;
    for (to_parse) |word, i| {
        for (word) |c, j| {
            switch (state) {
                .vertex_idx => {
                    switch (c) {
                        '0'...'9' => {
                            const num = c - '0';
                            idx_to_put = idx_to_put * 10 + num;
                        },
                        '/' => {
                            if (idx_to_put > 0) {
                                vertex_indices[i] = idx_to_put - 1;
                            }
                            state = .normal_idx;
                            idx_to_put = 0;
                        },
                        else => return error.UnexpectedSymbol,
                    }
                },
                .normal_idx => {
                    switch (c) {
                        '0'...'9' => {
                            const num = c - '0';
                            idx_to_put = idx_to_put * 10 + num;
                        },
                        '/' => {
                            if (idx_to_put > 0) {
                                const normal = normals.items[idx_to_put - 1];

                                tri_normals[i] = normal;
                            }
                            state = .uv_idx;
                            idx_to_put = 0;
                        },
                        else => return error.UnexpectedSymbol,
                    }
                },
                .uv_idx => {
                    switch (c) {
                        '0'...'9' => {
                            const num = c - '0';
                            idx_to_put = idx_to_put * 10 + num;
                        },
                        else => return error.UnexpectedSymbol,
                    }

                    if (j == word.len - 1) {
                        const uv = uvs.items[idx_to_put - 1];
                        tri_uvs[i] = uv;
                    }
                },
            }
        }

        state = .vertex_idx;
        idx_to_put = 0;
    }

    return Face{
        .indices = vertex_indices,
        .normals = tri_normals,
        .uvs = tri_uvs,
    };
}

fn parseFloat(word: []const u8) !f32 {
    var after_decimal = false;
    var negative = false;

    var int_result: u32 = 0;
    var after_decimal_count: u32 = 0;

    for (word) |c| {
        switch (c) {
            '-' => {
                negative = true;
            },
            '.' => {
                after_decimal = true;
            },
            '0'...'9' => {
                const num: u8 = c - '0';
                int_result = int_result * 10 + num;
            },
            else => {
                return error.UnexpectedSymbol;
            },
        }

        if (after_decimal) {
            after_decimal_count += 1;
        }
    }

    var float_result = @intToFloat(f32, int_result);

    var i: u32 = 0;
    while (i < after_decimal_count - 1) : (i += 1) {
        float_result /= 10;
    }

    if (negative) float_result *= -1;

    return float_result;
}
