// Given an independent variable range between a min and max value, and a dependent
// variable range between a min and max, allows interpolating values at integer steps within
// the independent variable range. Useful for interpolating values from one pixel to some other pixel
pub const LerpEdge = struct {
    ind: [2]i32,
    dep: [2]f32,

    // Lerp between dependent variable bounds. Step refers to how many integer steps we are
    // along the way between the independent variable bounds
    pub fn interpolateAt(self: LerpEdge, step: i32) f32 {
        if (self.ind[0] == self.ind[1]) return self.dep[0];

        const m = (self.dep[1] - self.dep[0]) / @intToFloat(f32, (self.ind[1] - self.ind[0]));
        var d = self.dep[0];

        return d + (m * @intToFloat(f32, step));
    }

    pub fn getMiddle(self: LerpEdge) i32 {
        return @divFloor((self.ind[1] + 1 - self.ind[0]), 2);
    }
};

pub const LerpEdge2 = struct {
    ind: [3]i32,
    dep: [3]f32,

    // Behaves as if two sets of interpolation bounds are concatenated
    // If step is between elements 0 and 1 of the independent variable bounds,
    // then lerp between elements 0 and 1 of the dependent variable bounds.
    // Otherwise, lerp between elements 1 and 2 of the dependent variable bounds
    pub fn interpolateAt(self: LerpEdge2, step: i32) f32 {
        // Max step between first two independent variable bounds
        const first_max_step = self.ind[1] - self.ind[0];

        if (step < first_max_step) {
            const bounds = LerpEdge{
                .ind = [2]i32{ self.ind[0], self.ind[1] },
                .dep = [2]f32{ self.dep[0], self.dep[1] },
            };
            return bounds.interpolateAt(step);
        }

        const new_step = step - first_max_step;

        const bounds = LerpEdge{
            .ind = [2]i32{ self.ind[1], self.ind[2] },
            .dep = [2]f32{ self.dep[1], self.dep[2] },
        };

        return bounds.interpolateAt(new_step);
    }
};

pub const LerpEdges = struct { edge1: LerpEdge, edge2: LerpEdge2 };

pub fn edgeLerp(ind0: i32, ind1: i32, ind2: i32, dep0: f32, dep1: f32, dep2: f32) LerpEdges {
    const edge1_ind = [2]i32{ ind0, ind2 };
    const edge1_dep = [2]f32{ dep0, dep2 };

    const edge1 = .{
        .ind = edge1_ind,
        .dep = edge1_dep,
    };

    const edge2_ind = [3]i32{ ind0, ind1, ind2 };
    const edge2_dep = [3]f32{ dep0, dep1, dep2 };

    const edge2 = .{
        .ind = edge2_ind,
        .dep = edge2_dep,
    };

    return .{
        .edge1 = edge1,
        .edge2 = edge2,
    };
}

pub fn lerp(a: f32, b: f32, t: f32) f32 {
    return a + (b - a) * t;
}

pub fn getPercentage(current: f32, min: f32, max: f32) f32 {
    if (max == min) return 0;

    return (current - min) / (max - min);
}
