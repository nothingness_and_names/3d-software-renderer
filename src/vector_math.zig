const std = @import("std");

pub const Vec2 = struct {
    x: i16,
    y: i16,
};

pub const Vec3 = struct {
    x: f32,
    y: f32,
    z: f32,

    pub fn dot(self: Vec3, v2: Vec3) f32 {
        return self.x * v2.x + self.y * v2.y + self.z * v2.z;
    }

    pub fn cross(self: Vec3, v2: Vec3) Vec3 {
        const x = self.y * v2.z - self.z * v2.y;
        const y = self.z * v2.x - self.x * v2.z;
        const z = self.x * v2.y - self.y * v2.x;

        return .{
            .x = x,
            .y = y,
            .z = z,
        };
    }

    pub fn subtract(self: Vec3, v2: Vec3) Vec3 {
        return .{
            .x = self.x - v2.x,
            .y = self.y - v2.y,
            .z = self.z - v2.z,
        };
    }

    pub fn multiply(self: Vec3, k: f32) Vec3 {
        return .{
            .x = k * self.x,
            .y = k * self.y,
            .z = k * self.z,
        };
    }

    pub fn add(self: Vec3, v2: Vec3) Vec3 {
        return .{
            .x = self.x + v2.x,
            .y = self.y + v2.y,
            .z = self.z + v2.z,
        };
    }

    pub fn length(self: Vec3) f32 {
        return @sqrt(self.dot(self));
    }

    pub fn normalize(self: Vec3) Vec3 {
        return self.multiply(1.0 / self.length());
    }

    pub fn toTranslationMatrix(self: Vec3) [4][4]f32 {
        return [4][4]f32{
            [4]f32{ 1, 0, 0, self.x },
            [4]f32{ 0, 1, 0, self.y },
            [4]f32{ 0, 0, 1, self.z },
            [4]f32{ 0, 0, 0, 1 },
        };
    }

    pub fn toHomogeneousVertex(self: Vec3) [4]f32 {
        return [4]f32{ self.x, self.y, self.z, 1 };
    }
};

pub const Plane = struct {
    normal: Vec3,
    distance: f32,

    pub fn computeIntersectionFraction(self: Plane, vec1: Vec3, vec2: Vec3) f32 {
        const numerator = -self.distance - self.normal.dot(vec1);
        const denominator = self.normal.dot(vec2.subtract(vec1));

        return numerator / denominator;
    }
};

// (Vsin(theta/2), cos(theta/2))
pub const Quaternion = struct {
    v: Vec3,
    w: f32,

    pub fn normalize(self: Quaternion) Quaternion {
        const dot_product = self.v.dot(self.v) + self.w * self.w;
        const length = @sqrt(dot_product);

        const new_v = self.v.multiply(1.0 / length);
        const new_w = self.w / length;

        return .{
            .v = new_v,
            .w = new_w,
        };
    }

    pub fn conjugate(self: Quaternion) Quaternion {
        return .{
            .v = .{
                .x = -self.v.x,
                .y = -self.v.y,
                .z = -self.v.z,
            },
            .w = self.w,
        };
    }

    pub fn rotateVector(self: Quaternion, vec: Vec3) Vec3 {
        return self
            .normalize()
            .hamiltonProduct(.{ .v = vec, .w = 0 })
            .hamiltonProduct(self.conjugate())
            .v;
    }

    // Product of two quaternions (hamilton product)
    // w1w2 - x1x2 - y1y2 - z1z2
    // + (w1x2 + x1w2 + y1z2 - z1y2)i
    // + (w1y2 - x1z2 + y1w2 + z1x2)j
    // + (w1z2 + x1y2 - y1x2 + z1w2)k
    pub fn hamiltonProduct(self: Quaternion, q2: Quaternion) Quaternion {
        const w1 = self.w;
        const w2 = q2.w;
        const x1 = self.v.x;
        const x2 = q2.v.x;
        const y1 = self.v.y;
        const y2 = q2.v.y;
        const z1 = self.v.z;
        const z2 = q2.v.z;

        const new_w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2;
        const new_x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2;
        const new_y = w1 * y2 - x1 * z2 + y1 * w2 + z1 * x2;
        const new_z = w1 * z2 + x1 * y2 - y1 * x2 + z1 * w2;

        return .{
            .v = .{
                .x = new_x,
                .y = new_y,
                .z = new_z,
            },
            .w = new_w,
        };
    }

    // quaternion to rotation matrix
    // [w*w+x*x-y*y-z*z, 2*(-w*z+x*y),    2*(w*y+x*z);
    //2*(w*z+x*y),     w*w-x*x+y*y-z*z, 2*(-w*x+y*z);
    //2*(-w*y+x*z),    2*(w*x+y*z),     w*w-x*x-y*y+z*z] / (w*w+x*x+y*y+z*z)
    pub fn toRotationMatrix(self: Quaternion) [4][4]f32 {
        const w = self.w;
        const x = self.v.x;
        const y = self.v.y;
        const z = self.v.z;

        const square_length = w * w + x * x + y * y + z * z;

        const a = (w * w + x * x - y * y - z * z) / square_length;
        const b = (2.0 * (-w * z + x * y)) / square_length;
        const c = (2.0 * (w * y + x * z)) / square_length;
        const d = (2.0 * (w * z + x * y)) / square_length;
        const e = (w * w - x * x + y * y - z * z) / square_length;
        const f = (2.0 * (-w * x + y * z)) / square_length;
        const g = (2.0 * (-w * y + x * z)) / square_length;
        const h = (2.0 * (w * x + y * z)) / square_length;
        const i = (w * w - x * x - y * y + z * z) / square_length;

        return [4][4]f32{
            [4]f32{ a, b, c, 0 },
            [4]f32{ d, e, f, 0 },
            [4]f32{ g, h, i, 0 },
            [4]f32{ 0, 0, 0, 1 },
        };
    }
};

pub fn createQuaternion(axis: Vec3, angle: f32) Quaternion {
    const v = .{
        .x = axis.x * @sin(angle / 2),
        .y = axis.y * @sin(angle / 2),
        .z = axis.z * @sin(angle / 2),
    };

    const w = @cos(angle / 2);

    return .{
        .v = v,
        .w = w,
    };
}

pub fn transposeMatrix44(matrix: [4][4]f32) [4][4]f32 {
    var result: [4][4]f32 = undefined;

    var i: u32 = 0;
    while (i < 4) : (i += 1) {
        var j: u32 = 0;
        while (j < 4) : (j += 1) {
            result[i][j] = matrix[j][i];
        }
    }

    return result;
}

pub fn multiplyMM4(mat1: [4][4]f32, mat2: [4][4]f32) [4][4]f32 {
    var result = [4][4]f32{
        [4]f32{ 0, 0, 0, 0 },
        [4]f32{ 0, 0, 0, 0 },
        [4]f32{ 0, 0, 0, 0 },
        [4]f32{ 0, 0, 0, 0 },
    };

    var i: u32 = 0;
    while (i < 4) : (i += 1) {
        var j: u32 = 0;
        while (j < 4) : (j += 1) {
            var k: u32 = 0;
            while (k < 4) : (k += 1) {
                result[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }

    return result;
}

pub fn multiplyMV4(matrix: [4][4]f32, vector: [4]f32) [4]f32 {
    var result = [4]f32{ 0, 0, 0, 0 };

    var i: u32 = 0;
    while (i < 4) : (i += 1) {
        var j: u32 = 0;
        while (j < 4) : (j += 1) {
            result[i] += matrix[i][j] * vector[j];
        }
    }

    return result;
}

pub fn homogeneousToVec(homogeneous: [4]f32) Vec3 {
    return .{
        .x = homogeneous[0],
        .y = homogeneous[1],
        .z = homogeneous[2],
    };
}
