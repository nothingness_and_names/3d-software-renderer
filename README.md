A 3D software renderer written in Zig, written while working through Gabriel Gambetta's book,
"[Computer Graphics From Scratch](https://www.gabrielgambetta.com/computer-graphics-from-scratch/)."

Currently contains a ray-tracer, and later on will implement a rasterizer.

## Ray-tracer

The ray-tracer has been extended somewhat beyond what was described in the book. Through multithreading and
some other optimizations, I managed to get the ray-tracer fast enough to do some basic real-time rendering (even 
without the use of the graphics card), with camera movement and rotation. 

In fairness, with more objects added in the scene it would quickly slow down, but I'm overall happy with the
results.

## Building

You'll need [Zig](https://ziglang.org/download), some version of libc installed, and you'll need SDL installed, but that's it.

### Debian Bullseye

Install gcc and SDL2 (if you don't have them): 
```
sudo apt install gcc libsdl2-2.0-0 libsdl2-dev
```

Then navigate to this project directory and run

```
zig build [flags]
```

The output will be in `zig-out/bin`.

If you want to build and run in one command (to test while developing, for example), then run:

```
zig build run
```

### Windows

1. [Download SDL2 development libraries here](https://www.libsdl.org/download-2.0.php). You'll want the Visual C++ version.

2. Create a folder called `lib` in the `C:\` directory.

3. Extract the SDL2 libraries you downloaded into the `C:\lib` folder you just created.

4. Rename the folder you just extracted to `SDL2`. You should now have a folder with the path `C:\lib\SDL2`, which contains subfolders `docs`, `include`, and `lib`.

5. Navigate back to this project directory and run `zig.exe build`. The output binary will be in the `zig-out\bin` folder.

To run the app, you'll also need to have `SDL2.dll` installed in the same directory as the binary. Assuming you followed the steps above, you can find `SDL2.dll` in `C:\lib\SDL2\lib\x64`. Copy it into the folder where the output binary is.

Now you can run the app. You should also be able to run it via the zig build command with `zig.exe build run`.
